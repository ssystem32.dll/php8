FROM php:8.1-fpm

ARG DEBIAN_FRONTEND=noninteractive
ARG EMAIL_ADDRESS
ARG NAME
RUN apt update
RUN apt install -y \
    tzdata \
    nginx \
    unzip \
    curl \
    git \
    wget \
    libfreetype6-dev \
    libmcrypt-dev \
    libpng-dev \
    libpq-dev \
    libcurl4-gnutls-dev \
    libxml2-dev \
    libzip-dev \
    libmemcached-dev \
    gnupg \
    acl \
    nano \
    supervisor

RUN pecl install igbinary
RUN pecl install redis && docker-php-ext-enable redis
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install pdo pdo_pgsql
RUN docker-php-ext-install gd
RUN docker-php-ext-install sockets
RUN docker-php-ext-configure zip
RUN docker-php-ext-install zip
RUN docker-php-ext-install opcache

COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/fpm-pool.conf /usr/local/etc/php-fpm.d/zz-docker.conf
COPY config/php.ini /usr/local/etc/php/conf.d/custom.ini
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN (curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer)
RUN wget https://get.symfony.com/cli/installer -O - | bash
RUN mv /root/.symfony5/bin/symfony /usr/local/bin/symfony
RUN mkdir -p /var/www/html

EXPOSE 8080
RUN git config --global user.email "$EMAIL_ADDRESS"
RUN git config --global user.name "$NAME"

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
